/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Lars Cormann
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.sondabar.beam.workshop._2;

import de.sondabar.beam.workshop.BasicPipeline;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;

public class LoadAndSavePipeline extends BasicPipeline {
    @Override
    protected Pipeline buildPipeline(Pipeline aPipeline) {
        aPipeline.apply("Load Data", TextIO.read().from(workingDir + "Test.txt"))
                 .apply("Save Data", TextIO.write().to("Hello.txt").withoutSharding());
        return aPipeline;
    }

    public static void main(String[] args) {
        final PipelineOptions options = PipelineOptionsFactory.create();
        new LoadAndSavePipeline().buildPipeline(Pipeline.create(options)).run().waitUntilFinish();
    }
}
