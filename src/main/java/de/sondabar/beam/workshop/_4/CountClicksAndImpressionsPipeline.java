/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Lars Cormann
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.sondabar.beam.workshop._4;

import de.sondabar.beam.workshop.BasicPipeline;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.join.CoGbkResult;
import org.apache.beam.sdk.transforms.join.CoGroupByKey;
import org.apache.beam.sdk.transforms.join.KeyedPCollectionTuple;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.beam.sdk.values.TypeDescriptors;

public class CountClicksAndImpressionsPipeline extends BasicPipeline {
    @Override
    protected Pipeline buildPipeline(Pipeline aPipeline) {
        final PCollection<KV<String, Long>> clickValues = aPipeline.apply("Load Clicks", TextIO.read()
                                                                                               .from(workingDir + "simple/Clicks.txt"))
                                                                   .apply("Count clicks", Count.perElement());

        final PCollection<KV<String, Long>> impressionValues = aPipeline.apply("Load Impressions", TextIO.read()
                                                                                                         .from(workingDir + "simple/Impressions.txt"))
                                                                        .apply("Count Impressions", Count.perElement());
        TupleTag<Long> clicks = new TupleTag<>();
        TupleTag<Long> impressions = new TupleTag<>();

        PCollection<KV<String, CoGbkResult>> campaignData = KeyedPCollectionTuple.of(clicks, clickValues)
                                                                                 .and(impressions, impressionValues)
                                                                                 .apply("Group per campaign", CoGroupByKey
                                                                                         .create());


        campaignData.apply(MapElements.into(TypeDescriptors.strings())
                                      .via((KV<String, CoGbkResult> element) -> String.format("%s,%d,%d", element.getKey(), element
                                              .getValue().getOnly(clicks, 0L), element
                                              .getValue().getOnly(impressions, 0L))))
                    .apply("Write result", TextIO.write().to("ClickAndImpressionCount.txt").withoutSharding());

        return aPipeline;
    }

    public static void main(String[] args) {
        final PipelineOptions options = PipelineOptionsFactory.create();
        new CountClicksAndImpressionsPipeline().buildPipeline(Pipeline.create(options)).run().waitUntilFinish();
    }
}
