/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Lars Cormann
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.sondabar.beam.workshop._6;

import com.sun.org.apache.xpath.internal.operations.Bool;
import de.sondabar.beam.workshop.BasicPipeline;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.WithKeys;
import org.apache.beam.sdk.transforms.join.CoGbkResult;
import org.apache.beam.sdk.transforms.join.CoGroupByKey;
import org.apache.beam.sdk.transforms.join.KeyedPCollectionTuple;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.beam.sdk.values.TypeDescriptors;

public class CountBasedOnBidsPipeline extends BasicPipeline {
    @Override
    protected Pipeline buildPipeline(Pipeline aPipeline) {
                aPipeline.apply("Load Bids", TextIO.read()
                                                   .from(workingDir + "complex/Bids.txt"))
                         .apply("Remove bid price", MapElements.into(TypeDescriptors
                                 .strings()).via((String element) -> element.replaceAll(",[0-9]*$", "")
                         )).apply("Key bids", WithKeys.of((String element) -> element));

        final PCollection<KV<String, Boolean>> clickValues = aPipeline.apply("Load Clicks", TextIO.read()
                                                                                     .from(workingDir + "complex/Clicks.txt"))
                                                         .apply("Key bids", WithKeys.of((String element) -> KV.of(element, Boolean.TRUE)));

        final PCollection<KV<String, Boolean>> impressionValues = aPipeline.apply("Load Impressions", TextIO.read()
                                                                                               .from(workingDir + "complex/Impressions.txt"));

        final PCollection<KV<String, Boolean>> conversionsValues = aPipeline.apply("Load Conversions", TextIO.read()
                                                                                                .from(workingDir + "complex/Conversions.txt"));

        TupleTag<Long> impressions = new TupleTag<>();
        TupleTag<Long> clicks = new TupleTag<>();
        TupleTag<Long> conversions = new TupleTag<>();

        PCollection<KV<String, CoGbkResult>> campaignData = KeyedPCollectionTuple.of(clicks, clickValues)
                                                                                 .and(impressions, impressionValues)
                                                                                 .and(conversions, conversionsValues)
                                                                                 .apply("Group per campaign", CoGroupByKey
                                                                                         .create());


        campaignData.apply(MapElements.into(TypeDescriptors.strings())
                                      .via((KV<String, CoGbkResult> element) -> String.format("%s,%d,%d,%d", element.getKey(), element
                                              .getValue().getOnly(clicks, 0L), element
                                              .getValue().getOnly(impressions, 0L), element
                                              .getValue().getOnly(conversions, 0L)
                                      )))
                    .apply("Write result", TextIO.write().to("EventCount.txt").withoutSharding());

        return aPipeline;
    }

    public static void main(String[] args) {
        final PipelineOptions options = PipelineOptionsFactory.create();
        new CountBasedOnBidsPipeline().buildPipeline(Pipeline.create(options)).run().waitUntilFinish();
    }
}
